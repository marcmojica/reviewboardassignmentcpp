// CollegeReviewBoardCPP.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <iomanip>

using namespace std;


enum SchoolType
{
	LIBERAL_ARTS,
	MUSIC
};

enum ReviewStatusType
{
	NOT_REVIEWED,
	ACCEPTED,
	REJECTED
};



const int MAX_LIBERAL_ARTS_SCHOOL = 5;
const int MAX_MUSIC_SCHOOL = 3;

const string GPA_TOO_LOW = "GPA too low";
const string SAT_VERBAL_TOO_LOW = "SAT Verbal too low";
const string SAT_MATH_TOO_LOW = "SAT Math too low";
const string TOTAL_SAT_TOO_LOW = "Total SAT Score too low";
const string MAX_ACCEPTANCE = "School is full";



class StudentRecord {

public:
	int id;
	SchoolType school;
	float gpa;
	int math;
	int verbal;
	bool alumnus;
	ReviewStatusType status;
	vector<string> rejectionReasons;

	StudentRecord() {
		status = ReviewStatusType::NOT_REVIEWED;
	}

	string GetSchoolName() {
		if (school == SchoolType::LIBERAL_ARTS)
			return "Liberal Arts";
		else
			return "Music";
	}

	int GetTotalSatScore() {
		return math + verbal;
	}

	static vector<StudentRecord> Load(string file)
	{
		ifstream infile(file);

		vector<StudentRecord> records;

		char schoolChar, alumnusChar;
		float gpa;
		int math, verbal;
		bool alumnus;
		SchoolType school;

		int count = 1;

		//L 4.0 600 650 N
		while (infile >> schoolChar >> gpa >> math >> verbal >> alumnusChar)
		{
			school = schoolChar == 'L' ? SchoolType::LIBERAL_ARTS : SchoolType::MUSIC;
			alumnus = alumnusChar == 'Y';

			StudentRecord record;
			record.id = count;
			record.school = school;
			record.gpa = gpa;
			record.math = math;
			record.verbal = verbal;
			record.alumnus = alumnus;
			record.status = ReviewStatusType::NOT_REVIEWED;

			count++;
			records.push_back(record);
		}
		return records;
	}
};



class ReviewBoard {

public:
	vector<StudentRecord> recordsReviewed;

	ReviewBoard() {

	}

	int GetMusicSchoolAccepted() {
		int total = 0;

		for (vector<StudentRecord>::iterator it = recordsReviewed.begin(); it != recordsReviewed.end(); ++it) {
			if (it->status == ReviewStatusType::ACCEPTED && it->school == SchoolType::MUSIC) {
				total++;
			}
		}

		return total;
	}

	int GetMusicSchoolRejected() {
		int total = 0;

		for (vector<StudentRecord>::iterator it = recordsReviewed.begin(); it != recordsReviewed.end(); ++it) {
			if (it->status == ReviewStatusType::REJECTED && it->school == SchoolType::MUSIC) {
				total++;
			}
		}

		return total;
	}

	int GetLiberalArtsAccepted() {
		int total = 0;

		for (vector<StudentRecord>::iterator it = recordsReviewed.begin(); it != recordsReviewed.end(); ++it) {
			if (it->status == ReviewStatusType::ACCEPTED && it->school == SchoolType::LIBERAL_ARTS) {
				total++;
			}
		}

		return total;
	}

	int GetLiberalArtsRejected() {
		int total = 0;

		for (vector<StudentRecord>::iterator it = recordsReviewed.begin(); it != recordsReviewed.end(); ++it) {
			if (it->status == ReviewStatusType::REJECTED && it->school == SchoolType::LIBERAL_ARTS) {
				total++;
			}
		}

		return total;
	}

	int GetTotalAccepted() {
		int total = 0;

		for (vector<StudentRecord>::iterator it = recordsReviewed.begin(); it != recordsReviewed.end(); ++it) {
			if (it->status == ReviewStatusType::ACCEPTED) {
				total++;
			}
		}

		return total;
	}

	int GetTotalRejected() {
		int total = 0;

		for (vector<StudentRecord>::iterator it = recordsReviewed.begin(); it != recordsReviewed.end(); ++it) {
			if (it->status == ReviewStatusType::REJECTED) {
				total++;
			}
		}

		return total;
	}

	int GetLiberalArtsReviewedTotal() {
		int total = 0;

		for (vector<StudentRecord>::iterator it = recordsReviewed.begin(); it != recordsReviewed.end(); ++it) {
			if (it->school == SchoolType::LIBERAL_ARTS) {
				total++;
			}
		}

		return total;
	}

	int GetMusicSchoolReviewedTotal() {
		int total = 0;

		for (vector<StudentRecord>::iterator it = recordsReviewed.begin(); it != recordsReviewed.end(); ++it) {
			if (it->school == SchoolType::MUSIC) {
				total++;
			}
		}

		return total;
	}


	void ReviewApplicants(vector<StudentRecord>& records)
	{
		for (vector<StudentRecord>::iterator it = records.begin(); it != records.end(); ++it) {
			ReviewApplicant(*it);
		}
	}

	ReviewStatusType ReviewApplicant(StudentRecord& record)
	{
		// start the review
		switch (record.school)
		{
		case SchoolType::LIBERAL_ARTS:
			record.status = IsEligibleForLiberalArtsSchool(record) ? ReviewStatusType::ACCEPTED : ReviewStatusType::REJECTED;
			break;
		case SchoolType::MUSIC:
			record.status = IsEligibleForMusicSchool(record) ? ReviewStatusType::ACCEPTED : ReviewStatusType::REJECTED;
			break;
		}

		recordsReviewed.push_back(record);

		return record.status;
	}



	/// <summary>
	/// No preferences for alumni here
	/// Math and verbal SAT�s must be at least 500.
	/// </summary>
	/// <returns></returns>
	bool IsEligibleForMusicSchool(StudentRecord& record)
	{
		// assume eligible until otherwise not
		bool result = true;

		// we test each scenario separately because we need to record rejection reason

		// Test to see if max accepted already; first come, first servered
		if (GetMusicSchoolAccepted() == MAX_MUSIC_SCHOOL)
		{
			result = false;
			record.rejectionReasons.push_back(MAX_ACCEPTANCE);
		}

		/// Math SAT must be at least 500.
		if (record.math < 500)
		{
			result = false;
			record.rejectionReasons.push_back(SAT_MATH_TOO_LOW);
		}

		/// Verbal SAT must be at least 500.
		if (record.verbal < 500)
		{
			result = false;
			record.rejectionReasons.push_back(SAT_VERBAL_TOO_LOW);
		}

		return result;
	}

	/// <summary>
	/// If a parent is an alumnus, the GPA must be at least 3.0, but if no parents are alumni the GPA must be at least 3.5.
	/// If a parent is an alumnus, the combined SAT score must be at least 1000, but if no parents are alumni the SAT must be at least 1200.
	/// </summary>
	/// <returns></returns>
	bool IsEligibleForLiberalArtsSchool(StudentRecord& record)
	{
		// assume eligible until otherwise not
		bool result = true;

		// we test each scenario separately because we need to record rejection reason

		// Test to see if max accepted already; first come, first servered
		if (GetLiberalArtsAccepted() == MAX_LIBERAL_ARTS_SCHOOL)
		{
			result = false;
			record.rejectionReasons.push_back(MAX_ACCEPTANCE);
		}

		if (record.alumnus)
		{
			// If a parent is an alumnus, the combined SAT score must be at least 1000
			if (record.GetTotalSatScore() < 1000)
			{
				result = false;
				record.rejectionReasons.push_back(TOTAL_SAT_TOO_LOW);
			}

			// if a parent is an alumnus, the GPA must be at least 3.0
			if (record.gpa < 3.0)
			{
				result = false;
				record.rejectionReasons.push_back(GPA_TOO_LOW);
			}
		}
		else
		{
			// if no parents are alumni the SAT must be at least 1200.
			if (record.GetTotalSatScore() < 1200)
			{
				result = false;
				record.rejectionReasons.push_back(TOTAL_SAT_TOO_LOW);
			}

			// if no parents are alumni the GPA must be at least 3.5
			if (record.gpa < 3.5)
			{
				result = false;
				record.rejectionReasons.push_back(GPA_TOO_LOW);
			}
		}

		return result;
	}

};


class GeneratorAssignment {
public:
	string Output(ReviewBoard& reviewBoard)
	{
		stringstream ss;

		//put arbitrary formatted data into the stream
		ss << "Acceptance to College by Marc Mojica\n\n";
		
		for (vector<StudentRecord>::iterator it = reviewBoard.recordsReviewed.begin(); it != reviewBoard.recordsReviewed.end(); ++it) {
			
			ss << "Applicant #: " << it->id << "\n";
			ss << "School = " << (it->school == SchoolType::LIBERAL_ARTS ? 'L' : 'M');
			ss << fixed;
			ss << setprecision(1);
			
			ss << " GPA = " << it->gpa;
			ss << " math = " << it->math;
			ss << " verbal = " << it->verbal;
			ss << " alumnus = " << (it->alumnus ? 'Y' : 'N') << "\n";
			ss << "Applying to " << it->GetSchoolName() << "\n";

			switch (it->status)
			{
				case ReviewStatusType::ACCEPTED:
					ss << "Accepted to " << it->GetSchoolName() << "!!\n";
					break;
				case ReviewStatusType::REJECTED:
					for (vector<string>::iterator reason = it->rejectionReasons.begin(); reason != it->rejectionReasons.end(); ++reason) {
						ss << "Rejected - " << *reason << "\n";
					}
					break;
			}

			ss << "*******************************\n";
		}

		ss << "There were " << reviewBoard.recordsReviewed.size() << " in the file\n";
		ss << "There were " << reviewBoard.GetLiberalArtsAccepted() << " acceptances to Liberal Arts\n";
		ss << "There were " << reviewBoard.GetMusicSchoolAccepted() << " acceptances to Music\n\n";
		ss << "Press any key to continue\n";

		//convert the stream buffer into a string
		string str = ss.str();
		return str;
	}
};

int main()
{
	vector<StudentRecord> records = StudentRecord::Load("mp3accept.txt");

	ReviewBoard reviewBoard;
	reviewBoard.ReviewApplicants(records);

	GeneratorAssignment outputGenerator;
	cout << outputGenerator.Output(reviewBoard);

	std::cin.get();
	return 0;
}